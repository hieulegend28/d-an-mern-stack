import { GET_USER } from '../actions'

const user_reducer = (state, action) => {
  if (action.type === GET_USER) {
    const { listUsers, user } = action.payload
    console.log(listUsers)
    const singleUser = listUsers.find((x) => x.email === user.email)
    if (!singleUser) {
      return {
        ...state,
        success: true,
        users: listUsers,
        singleUser: user,
      }
    }
    return {
      ...state,
      success: true,
      users: listUsers,
      singleUser: singleUser,
    }
  }

  throw new Error(`No Matching "${action.type}" - action type`)
}

export default user_reducer
