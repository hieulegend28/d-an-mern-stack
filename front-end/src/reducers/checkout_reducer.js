import {
  GET_INFOR_CHECKOUT,
  ADD_TO_BILL_DETAIL,
  UPDATE_INFOR_CHECKOUT,
} from '../actions'
const checkout_reducer = (state, action) => {
  if (action.type === GET_INFOR_CHECKOUT) {
    const { bills, cart, total_amount, total_items } = action.payload

    return {
      ...state,
      bills: bills,
      billDetail: {
        ...state.billDetail,
        cart: cart,
        totalBill: total_amount,
        count: total_items,
      },
    }
  }

  if (action.type === UPDATE_INFOR_CHECKOUT) {
    const { singleUser, cart, total_amount, total_items } = action.payload
    return {
      ...state,
      billDetail: {
        ...state.billDetail,
        cart: cart,
        totalBill: total_amount,
        count: total_items,
        address: singleUser.address,
        firstName:
          singleUser.fullName.split(' ')[
            singleUser.fullName.split(' ').length - 1
          ],
        lastName:
          singleUser.fullName.split(' ')[0] +
          ' ' +
          singleUser.fullName.split(' ')[1],
        phone: singleUser.phoneNumber,
      },
    }
  }

  if (action.type === ADD_TO_BILL_DETAIL) {
    const { data, name } = action.payload
    return {
      ...state,
      billDetail: { ...state.billDetail, [name]: data },
    }
  }
  throw new Error(`No Matching "${action.type}" - action type`)
}

export default checkout_reducer
