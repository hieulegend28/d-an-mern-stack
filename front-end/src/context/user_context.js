import React, { useContext, useEffect, useReducer } from 'react'
import { useAuth0 } from '@auth0/auth0-react'
import axios from 'axios'
import reducer from '../reducers/user_reducer'
import { GET_USER } from '../actions'

const UserContext = React.createContext()
const initialState = {
  users: [],
  singleUser: null,
  success: false,
  errorr: false,
}
export const UserProvider = ({ children }) => {
  const { loginWithRedirect, logout, user } = useAuth0()
  const [state, dispatch] = useReducer(reducer, initialState)
  console.log(user)

  const fetchUsers = async () => {
    try {
      const response = await axios.post(
        'http://localhost:5000/api/auth/login',
        {
          userName: user.nickname,
          email: user.email,
          imageUser: user.picture,
          sub: user.sub.split('|')[0],
        }
      )
      const listUsers = response.data
      dispatch({ type: GET_USER, payload: { listUsers, user } })
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    fetchUsers()
  }, [user])

  return (
    <UserContext.Provider value={{ loginWithRedirect, logout, ...state }}>
      {children}
    </UserContext.Provider>
  )
}
// make sure use
export const useUserContext = () => {
  return useContext(UserContext)
}
