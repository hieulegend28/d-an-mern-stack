import { createContext, useContext, useEffect, useReducer } from 'react'
import reducer from '../reducers/checkout_reducer'
import { useUserContext } from './user_context'
import {
  GET_INFOR_CHECKOUT,
  ADD_TO_BILL_DETAIL,
  UPDATE_INFOR_CHECKOUT,
} from '../actions'
import { useCartContext } from './cart_context'
import axios from 'axios'

const initialState = {
  bills: [],
  billDetail: {
    firstName: '',
    lastName: '',
    address: '',
    phone: '',
    remember: false,
    totalBill: 0,
    count: 0,
    cart: [],
  },
}

const CheckoutContext = createContext()

export const CheckoutProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const { singleUser } = useUserContext()
  const { cart, total_items, total_amount } = useCartContext()
  const addToBillDetail = (data, name) => {
    dispatch({
      type: ADD_TO_BILL_DETAIL,
      payload: { data, name },
    })
  }
  const updateBill = () => {
    dispatch({
      type: UPDATE_INFOR_CHECKOUT,
      payload: { singleUser, cart, total_amount, total_items },
    })
  }

  const fetchBills = async () => {
    try {
      const response = await axios.get('http://localhost:5000/api/bill')
      const bills = response.data
      dispatch({
        type: GET_INFOR_CHECKOUT,
        payload: { singleUser, cart, total_amount, total_items, bills },
      })
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    fetchBills()
  }, [singleUser])
  return (
    <CheckoutContext.Provider value={{ ...state, addToBillDetail, updateBill }}>
      {children}
    </CheckoutContext.Provider>
  )
}
// make sure use
export const useCheckoutContext = () => {
  return useContext(CheckoutContext)
}
