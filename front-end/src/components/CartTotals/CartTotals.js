import React from 'react'
import './CartTotals.scss'
import { useCartContext } from '../../context/cart_context'
import { useUserContext } from '../../context/user_context'
import { formatPrice } from '../../constants/helpers'
import { Link } from 'react-router-dom'
import { useCheckoutContext } from '../../context/checkout_context'

const CartTotals = () => {
  const { total_amount, shipping_fee } = useCartContext()
  const { updateBill } = useCheckoutContext()
  const { singleUser: myUser, loginWithRedirect } = useUserContext()

  return (
    <section className="carttotal">
      <div>
        <article>
          <h5>
            Tổng : <span>{formatPrice(total_amount)}</span>
          </h5>
          <h5>
            Phí ship : <span>{formatPrice(shipping_fee)}</span>
          </h5>
          <hr />
          <h4>
            Tổng bill : <span>{formatPrice(total_amount + shipping_fee)}</span>
          </h4>
        </article>
        {myUser ? (
          <Link onClick={() => updateBill()} to="/checkout" className="btn">
            Tiếp tục thanh toán
          </Link>
        ) : (
          <button className="btn" type="button" onClick={loginWithRedirect}>
            Đăng nhập
          </button>
        )}
      </div>
    </section>
  )
}

export default CartTotals
