import * as React from 'react'
import Typography from '@mui/material/Typography'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import Grid from '@mui/material/Grid'
import { useCheckoutContext } from '../../context/checkout_context'
import { formatPrice } from '../../constants/helpers'

export default function Review() {
  const { billDetail } = useCheckoutContext()
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Tóm tắt đơn hàng
      </Typography>
      <List disablePadding>
        {billDetail.cart.map((product) => (
          <ListItem key={product._id} sx={{ py: 1, px: 0 }}>
            <ListItemText
              primary={product.name + ' X ' + product.amount}
              secondary={
                product.category.categoryName === 'Bàn phím cơ'
                  ? product.switchs
                  : ''
              }
            />
            <Typography variant="body2">
              {formatPrice(product.price)}
            </Typography>
          </ListItem>
        ))}

        <ListItem sx={{ py: 1, px: 0 }}>
          <ListItemText primary="Tổng" />
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            {formatPrice(billDetail.totalBill)}
          </Typography>
        </ListItem>
      </List>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" gutterBottom sx={{ mt: 2 }}>
            Thông tin gửi hàng
          </Typography>
          <Typography gutterBottom>
            {billDetail.lastName + ' ' + billDetail.firstName}
          </Typography>
          <Typography gutterBottom>{billDetail.address}</Typography>
        </Grid>
        <Grid item container direction="column" xs={12} sm={6}>
          <Typography variant="h6" gutterBottom sx={{ mt: 2 }}>
            Phương thức thanh toán
          </Typography>
          <Grid container>Thanh toán khi nhận hàng</Grid>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}
