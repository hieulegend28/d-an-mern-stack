import * as React from 'react'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import TextField from '@mui/material/TextField'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import { useCheckoutContext } from '../../context/checkout_context'

export default function AddressForm() {
  const { billDetail, addToBillDetail } = useCheckoutContext()

  console.log(billDetail)
  function handle(e) {
    const name = e.target.id
    const data = e.target.value
    addToBillDetail(data, name)
  }

  function handleCheckBox(e) {
    const name = e.target.id
    const data = e.target.checked
    addToBillDetail(data, name)
  }
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Thông tin gửi hàng
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="firstName"
            name="firstName"
            label="Tên"
            fullWidth
            autoComplete="given-name"
            variant="standard"
            defaultValue={billDetail.firstName}
            onChange={(e) => handle(e)}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="lastName"
            name="lastName"
            label="Họ lót"
            fullWidth
            autoComplete="family-name"
            variant="standard"
            defaultValue={billDetail.lastName}
            onChange={(e) => handle(e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="address"
            name="address1"
            label="Địa chỉ"
            fullWidth
            autoComplete="shipping address-line1"
            variant="standard"
            defaultValue={billDetail.address}
            onChange={(e) => handle(e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="phone"
            name="phone"
            label="Số điện thoại"
            type="number"
            fullWidth
            autoComplete="shipping address-line1"
            variant="standard"
            defaultValue={billDetail.phone}
            onChange={(e) => handle(e)}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            control={
              <Checkbox
                color="secondary"
                id="remember"
                name="remember"
                checked={billDetail.remember}
                onChange={(e) => handleCheckBox(e)}
              />
            }
            label="Sử dụng địa chỉ này để thanh toán cho lần sau"
          />
        </Grid>
      </Grid>
    </React.Fragment>
  )
}
