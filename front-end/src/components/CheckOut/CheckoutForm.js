import * as React from 'react'
import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Paper from '@mui/material/Paper'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import AddressForm from './AddressForm'
import Review from './Review'
import { useCheckoutContext } from '../../context/checkout_context'
import { useCartContext } from '../../context/cart_context'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import { useUserContext } from '../../context/user_context'

const steps = ['Địa chỉ gửi hàng', 'Xem lại đơn hàng']

function getStepContent(step) {
  switch (step) {
    case 0:
      return <AddressForm />
    case 1:
      return <Review />
    default:
      throw new Error('Unknown step')
  }
}

const theme = createTheme()

function CheckoutForm() {
  const [activeStep, setActiveStep] = React.useState(0)
  const { billDetail } = useCheckoutContext()
  const { singleUser } = useUserContext()
  const { clearCart } = useCartContext()
  const history = useNavigate()
  const handleNext = () => {
    if (
      billDetail.firstName === '' ||
      billDetail.phone === '' ||
      billDetail.lastName === '' ||
      billDetail.address === ''
    ) {
      alert('Vui lòng nhập đầy đủ thông tin')
    } else {
      console.log(typeof billDetail.firstName)
      setActiveStep(activeStep + 1)
    }
  }

  const handleBack = () => {
    setActiveStep(activeStep - 1)
  }

  const handleSubmit = async () => {
    console.log(billDetail)
    setActiveStep(activeStep + 1)
    await axios.post('http://localhost:5000/api/bill/create', {
      user: singleUser._id,
      product: billDetail.cart,
      total: billDetail.totalBill,
      count: billDetail.count,
    })
    for (let i = 0; i < billDetail.cart.length; i++) {
      await axios.post('http://localhost:5000/api/product/update', {
        _id: billDetail.cart[i]._id.split('#')[0],
        countInStock: billDetail.cart[i].max - billDetail.cart[i].amount,
      })
    }

    if (billDetail.remember === true) {
      await axios.post('http://localhost:5000/api/auth/update', {
        _id: singleUser._id,
        fullName: billDetail.lastName + ' ' + billDetail.firstName,
        phoneNumber: billDetail.phone,
        address: billDetail.address,
        remember: billDetail.remember,
      })
    }
    setTimeout(() => {
      history('/')
      clearCart()
    }, 3000)
  }

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="sm" sx={{ mb: 4 }}>
        <Paper
          variant="outlined"
          sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}
        >
          <Typography component="h1" variant="h4" align="center">
            Thanh toán
          </Typography>
          <Stepper activeStep={activeStep} sx={{ pt: 3, pb: 5 }}>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography variant="h5" gutterBottom>
                  Đặt hàng thành công.
                </Typography>
                <Typography variant="subtitle1">
                  Cảm ơn bạn đã ghé thăm và mua sản phẩm bên trang web chúng
                  tôi. Đơn hàng sẽ đến tay bạn trong 1 đến 2 ngày tới. Chúc bạn
                  có một trải nghiệm mua hàng tuyệt vời.
                </Typography>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {getStepContent(activeStep)}
                <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
                  {activeStep !== 0 && (
                    <Button onClick={handleBack} sx={{ mt: 3, ml: 1 }}>
                      Trở lại
                    </Button>
                  )}
                  {activeStep === steps.length - 1 ? (
                    <Button
                      variant="contained"
                      onClick={handleSubmit}
                      sx={{ mt: 3, ml: 1 }}
                    >
                      Xác nhận
                    </Button>
                  ) : (
                    <Button
                      variant="contained"
                      onClick={handleNext}
                      sx={{ mt: 3, ml: 1 }}
                    >
                      Tiếp theo
                    </Button>
                  )}
                </Box>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
      </Container>
    </ThemeProvider>
  )
}

export default CheckoutForm
