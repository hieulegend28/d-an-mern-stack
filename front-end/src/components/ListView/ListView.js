import React from 'react'
import './ListView.scss'
import { formatPrice } from '../../constants/helpers'
import { Link } from 'react-router-dom'
const ListView = ({ products }) => {
  return (
    <section className="listviews">
      {products.map((product) => {
        const { _id, images, name, price, description } = product
        return (
          <article key={_id}>
            <Link to={`/products/${_id}`}>
              <img src={images[0]} alt={name} />
            </Link>
            <div>
              <h4>{name}</h4>
              <h5 className="price">{formatPrice(price)}</h5>
              <p>{description.substring(0, 150)}...</p>
              <Link to={`/products/${_id}`} className="btn">
                Chi tiết
              </Link>
            </div>
          </article>
        )
      })}
    </section>
  )
}

export default ListView
