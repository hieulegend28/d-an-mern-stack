import React from 'react'
import './Filters.scss'
import { useFilterContext } from '../../context/filter_context'
import { getUniqueValues, formatPrice } from '../../constants/helpers'
import { FaCheck } from 'react-icons/fa'

const Filters = () => {
  const {
    filters: {
      text,
      category,
      brand,
      color,
      styleSwitch,
      min_price,
      price,
      max_price,
      shipping,
    },
    updateFilters,
    clearFilters,
    all_products,
  } = useFilterContext()

  const categories = getUniqueValues(all_products, 'category')
  const switchs = getUniqueValues(all_products, 'styleSwitch')
  const brands = getUniqueValues(all_products, 'brand')
  const colors = getUniqueValues(all_products, 'colors')

  console.log(category)

  return (
    <section className="filters">
      <div className="content">
        <form onSubmit={(e) => e.preventDefault()}>
          {/* Search Input */}
          <div className="form-control">
            <input
              type="text"
              name="text"
              placeholder="search"
              className="search-input"
              value={text}
              onChange={updateFilters}
            />
          </div>
        </form>
      </div>
      {/* End Of Search Input */}

      {/* Categories */}
      <div className="form-control">
        <h5>danh mục</h5>
        <div>
          {categories.map((c, index) => {
            return (
              <button
                key={index}
                onClick={updateFilters}
                type="button"
                name="category"
                style={{ fontFamily: 'Lora, serif', fontSize: '0.8rem' }}
                className={`${
                  category.toLowerCase() === c.toLowerCase() ? 'active' : null
                }`}
              >
                {c}
              </button>
            )
          })}
        </div>
      </div>
      {/* End Of Categories */}

      {/* Switch */}
      <div className="form-control">
        <h5>Loại switch</h5>
        <div>
          {switchs.map((c, index) => {
            return (
              <button
                key={index}
                onClick={updateFilters}
                type="button"
                name="styleSwitch"
                style={{ fontFamily: 'Lora, serif', fontSize: '0.8rem' }}
                className={`${
                  styleSwitch.toLowerCase() === c.toLowerCase()
                    ? 'active'
                    : null
                }`}
              >
                {c}
              </button>
            )
          })}
        </div>
      </div>
      {/* End Of Switch */}

      {/* Companies */}
      <div className="form-control">
        <h5>Thương hiệu</h5>
        <select
          name="brand"
          value={brand}
          onChange={updateFilters}
          className="brand"
        >
          {brands.map((c, index) => {
            return (
              <option key={index} value={c}>
                {c}
              </option>
            )
          })}
        </select>
      </div>
      {/* End Of Companies */}

      {/* Colors */}
      <div className="form-control">
        <h5>Màu sắc</h5>
        <div className="colors">
          {colors.map((c, index) => {
            if (c === 'Tất cả') {
              return (
                <button
                  key={index}
                  name="color"
                  onClick={updateFilters}
                  data-color="Tất cả"
                  className={`${
                    color === 'Tất cả' ? 'all-btn active' : 'all-btn'
                  }`}
                >
                  Tất cả
                </button>
              )
            }

            return (
              <button
                key={index}
                name="color"
                style={{ background: c }}
                className={`${color === c ? 'color-btn active' : 'color-btn'}`}
                data-color={c}
                onClick={updateFilters}
              >
                {color === c ? (
                  <FaCheck
                    style={
                      color === '#ffffff'
                        ? { color: 'black' }
                        : { color: 'white' }
                    }
                  />
                ) : null}
              </button>
            )
          })}
        </div>
      </div>
      {/* End Of Colors */}

      {/* Price */}
      <div className="form-control">
        <h5>Giá từ</h5>
        <p className="price">{formatPrice(price)}</p>
        <input
          type="range"
          name="price"
          onChange={updateFilters}
          min={min_price}
          max={max_price}
          value={price}
        />
      </div>
      {/* End Of Price */}

      {/* Shipping */}
      <div className="form-control shipping">
        <label htmlFor="shipping">Miễn phí ship</label>
        <input
          type="checkbox"
          name="shipping"
          id="shipping"
          onChange={updateFilters}
          checked={shipping}
        />
      </div>
      {/* End Of Shipping */}

      <button type="button" className="clear-btn" onClick={clearFilters}>
        Bỏ chọn all
      </button>
    </section>
  )
}

export default Filters
