import React, { useState } from 'react'
import './ProductImages.scss'

const ProductImages = (images) => {
  const [main, setMain] = useState(images.images[0])
  console.log(images.images[0])
  return (
    <section className="product-images">
      <img src={main} alt="main" className="mainimg" />
      <div className="gallery">
        {images.images.map((image, index) => {
          console.log(index)
          return (
            <img
              src={image}
              alt=""
              key={index}
              onClick={() => setMain(images.images[index])}
              className={`${image === main ? 'active' : null}`}
            />
          )
        })}
      </div>
    </section>
  )
}

export default ProductImages
