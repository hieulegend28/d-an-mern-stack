import React from 'react'
import { useFilterContext } from '../../context/filter_context'
import { BsFillGridFill, BsList } from 'react-icons/bs'
import './Sort.scss'
const Sort = () => {
  const {
    filtered_products: products,
    grid_view,
    setGridView,
    setListView,
    sort,
    updateSort,
  } = useFilterContext()
  return (
    <section className="sortproduct">
      <div className="btn-container">
        <button
          type="button"
          className={`${grid_view ? 'active' : null}`}
          onClick={setGridView}
        >
          <BsFillGridFill />
        </button>
        <button
          type="button"
          className={`${!grid_view ? 'active' : null}`}
          onClick={setListView}
        >
          <BsList />
        </button>
      </div>
      <p>{products.length} sản phẩm được tìm thấy</p>
      <hr />
      <form>
        <label htmlFor="sort">Sắp xếp theo</label>
        <select
          name="sort"
          id="sort"
          className="sort-input"
          value={sort}
          onChange={updateSort}
        >
          <option value="price-lowest">Giá (Thấp nhất)</option>
          <option value="price-highest">Giá (Cao nhất)</option>
          <option value="name-a">Tên (a-z)</option>
          <option value="name-z">Tên (z-a)</option>
        </select>
      </form>
    </section>
  )
}

export default Sort
