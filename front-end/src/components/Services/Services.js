import React from 'react'
import './Services.scss'
import { services } from '../../constants/constants'

const Services = () => {
  return (
    <div className="services">
      <div className="section-center">
        <article className="header">
          <h3>
            Tìm Ra Bàn Phím <br />
            Dành Riêng Cho Bạn
          </h3>
          <p>
            Bàn phím của chúng tôi luôn mang lại cho bạn sự thoải mái và trải
            nghiệm chơi game tốt nhất.
          </p>
        </article>
        <div className="services-center">
          {services.map((service) => {
            const { id, icon, title, text } = service
            return (
              <article key={id} className="service">
                <span className="icon">{icon}</span>
                <h4>{title}</h4>
                <p>{text}</p>
              </article>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default Services
