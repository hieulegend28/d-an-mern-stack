import React from 'react'
import './CartItem.scss'
import { formatPrice } from '../../constants/helpers'
import AmountButtons from '../AmountButtons/AmountButtons'
import { FaTrash } from 'react-icons/fa'
import { useCartContext } from '../../context/cart_context'
const CartItem = ({
  _id,
  image,
  name,
  switchs,
  category,
  color,
  price,
  amount,
}) => {
  const { removeItem, toggleAmount } = useCartContext()
  const increase = () => {
    toggleAmount(_id, 'inc')
  }

  const decrease = () => {
    toggleAmount(_id, 'dec')
  }

  return (
    <div className="cartitem">
      <div className="title">
        <img src={image} alt={name} />
        <div>
          <h5 className="name">{name}</h5>
          <p className="color">
            Màu sắc :{' '}
            <span
              style={{ background: color, border: '1px solid black' }}
            ></span>
          </p>
          {category.categoryName === 'Bàn phím cơ' && (
            <p className="color">
              Switch :{' '}
              <span
                style={{
                  fontSize: '0.7rem',
                  width: '120px',
                  fontWeight: '900',
                }}
              >
                {switchs}
              </span>
            </p>
          )}

          <h5 className="price-small">{formatPrice(price)}</h5>
        </div>
      </div>

      <h5 className="price">{formatPrice(price)}</h5>
      <AmountButtons amount={amount} increase={increase} decrease={decrease} />
      <h5 className="subtotal">{formatPrice(price * amount)}</h5>
      <button
        type="button"
        className="remove-btn"
        onClick={() => removeItem(_id)}
      >
        <FaTrash />
      </button>
    </div>
  )
}

export default CartItem
