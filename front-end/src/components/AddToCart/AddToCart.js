import React, { useState } from 'react'
import './AddToCart.scss'
import { Link } from 'react-router-dom'
import { FaCheck } from 'react-icons/fa'
import { useCartContext } from '../../context/cart_context'
import AmountButtons from '../AmountButtons/AmountButtons'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'

const AddToCart = ({ product }) => {
  const { addToCart } = useCartContext()
  const { _id, countInStock, category, colors, styleSwitch } = product

  const [mainColor, setMainColor] = useState(colors[0])
  const [amount, setAmount] = useState(1)
  const [mainSwitch, setMainSwitch] = useState(styleSwitch[0])

  const increase = () => {
    setAmount((oldAmount) => {
      let tempAmount = oldAmount + 1
      if (tempAmount > countInStock) {
        tempAmount = countInStock
      }
      return tempAmount
    })
  }

  const decrease = () => {
    setAmount((oldAmount) => {
      let tempAmount = oldAmount - 1
      if (tempAmount < 1) {
        tempAmount = 1
      }
      return tempAmount
    })
  }

  return (
    <section className="addtocart">
      <div className="colors">
        <span>Màu sắc : </span>
        <div>
          {colors.map((color, index) => {
            return (
              <button
                key={index}
                style={{ background: color }}
                className={`${
                  mainColor === color ? 'color-btn active' : 'color-btn'
                }`}
                onClick={() => setMainColor(color)}
              >
                {mainColor === color ? (
                  <FaCheck
                    style={
                      color === '#ffffff'
                        ? { color: 'black' }
                        : { color: 'white' }
                    }
                  />
                ) : null}
              </button>
            )
          })}
        </div>
      </div>
      {category && category.categoryName === 'Bàn phím cơ' && (
        <p className="info">
          <span className="lable" style={{ marginTop: '9px' }}>
            Loại Switch :{' '}
          </span>
          {styleSwitch && styleSwitch.length > 1 ? (
            <FormControl>
              <RadioGroup
                aria-labelledby="demo-radio-buttons-group-label"
                defaultValue="female"
                name="radio-buttons-group"
                required="true"
              >
                {styleSwitch.map((item, index) => (
                  <FormControlLabel
                    key={index}
                    value={item}
                    classes={{}}
                    control={<Radio />}
                    label={item}
                    checked={mainSwitch === item ? true : false}
                    onClick={() => setMainSwitch(item)}
                  />
                ))}
              </RadioGroup>
            </FormControl>
          ) : (
            <span style={{ marginTop: '9px' }}>
              {styleSwitch && styleSwitch[0]}
            </span>
          )}
        </p>
      )}

      <div className="btn-container">
        <AmountButtons
          amount={amount}
          increase={increase}
          decrease={decrease}
        />
        <Link
          to="/cart"
          className="btn"
          onClick={() =>
            addToCart(_id, mainColor, mainSwitch, amount, category, product)
          }
        >
          Thêm vào giỏ
        </Link>
      </div>
    </section>
  )
}

export default AddToCart
