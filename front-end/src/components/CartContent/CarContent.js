import React from 'react'
import './CartContent.scss'
import { useCartContext } from '../../context/cart_context'
import { Link } from 'react-router-dom'
import CartColumns from '../CartColumns/CartColumns'
import CartItem from '../CartItem/CartItem'
import CartTotals from '../CartTotals/CartTotals'

const CartContent = () => {
  const { cart, clearCart } = useCartContext()
  console.log(cart)
  return (
    <section className="section section-center">
      <CartColumns />
      {cart.map((item) => {
        return <CartItem key={item._id} {...item} />
      })}
      <hr />
      <div className="link-container">
        <Link to="/products" className="link-btn">
          Tiếp tục mua sắm
        </Link>
        <button
          type="button"
          className="link-btn clear-btn"
          onClick={clearCart}
        >
          Xóa giỏ hàng
        </button>
      </div>
      <CartTotals />
    </section>
  )
}

export default CartContent
