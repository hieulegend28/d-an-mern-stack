import React from 'react'
import './user-info.scss'
import { useUserContext } from '../../../context/user_context'
const UserInfo = () => {
  const { singleUser: myUser } = useUserContext()

  return (
    <div className="user-info">
      <div className="user-info__img">
        <img
          src={
            myUser
              ? myUser.imageUser
              : 'https://www.kindpng.com/picc/m/22-223863_no-avatar-png-circle-transparent-png.png'
          }
          alt=""
        />
      </div>
      <div className="user-info__name">
        <span>{myUser && myUser.userName}</span>
      </div>
    </div>
  )
}

export default UserInfo
