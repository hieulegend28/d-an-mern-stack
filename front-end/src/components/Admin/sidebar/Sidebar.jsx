import React, { useEffect, useState } from 'react'
import './sidebar.scss'
import { Link, useLocation } from 'react-router-dom'
import sidebarNav from '../../../configs/sidebarNav'
import { useUserContext } from '../../../context/user_context'

const Sidebar = () => {
  const { logout } = useUserContext()

  const [activeIndex, setActiveIndex] = useState(0)
  const location = useLocation()

  useEffect(() => {
    const curPath = window.location.pathname.split('/admin')[1]

    const activeItem = sidebarNav.findIndex((item) => item.section === curPath)

    setActiveIndex(curPath.length === 0 ? 0 : activeItem)
  }, [location])

  const closeSidebar = () => {
    document.querySelector('.main__content').style.transform =
      'scale(1) translateX(0)'
    setTimeout(() => {
      document.body.classList.remove('sidebar-open')
      document.querySelector('.main__content').style = ''
    }, 500)
  }

  return (
    <div className="sidebar">
      <div className="sidebar__logo">
        <a href="http://localhost:3000">
          <img
            src="https://fv9-6.failiem.lv/thumb_show.php?i=fesk26t76&view"
            alt=""
          />
        </a>

        <div className="sidebar-close" onClick={closeSidebar}>
          <i className="bx bx-x"></i>
        </div>
      </div>
      <div className="sidebar__menu">
        {sidebarNav.map((nav, index) => (
          <Link
            to={nav.link}
            key={`nav-${index}`}
            className={`sidebar__menu__item ${
              activeIndex === index && 'active'
            }`}
            onClick={closeSidebar}
          >
            <div className="sidebar__menu__item__icon">{nav.icon}</div>
            <div className="sidebar__menu__item__txt">{nav.text}</div>
          </Link>
        ))}
        <div className="sidebar__menu__item">
          <div className="sidebar__menu__item__icon">
            <i className="bx bx-log-out"></i>
          </div>
          <div
            onClick={() => {
              logout({ returnTo: window.location.origin })
            }}
            className="sidebar__menu__item__txt"
          >
            Đăng xuất
          </div>
        </div>
      </div>
    </div>
  )
}

export default Sidebar
