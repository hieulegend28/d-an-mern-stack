import * as React from 'react'
import { FaShoppingCart, FaUserMinus, FaUserPlus } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import './CartButtons.scss'
import { useCartContext } from '../../context/cart_context'
import { useUserContext } from '../../context/user_context'
import Button from '@mui/material/Button'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'

const CartButtons = () => {
  const { total_items, clearCart } = useCartContext()
  const { loginWithRedirect, singleUser: myUser, logout } = useUserContext()
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <div className="cart-btn-wrapper">
      <Link to="/cart" className="cart-btn">
        Giỏ hàng
        <span className="cart-container">
          <FaShoppingCart />
          <span className="cart-value">{total_items}</span>
        </span>
      </Link>
      {myUser ? (
        <>
          <div className="userinfo">
            <div style={{ cursor: 'pointer' }} className="fix-ui-user">
              <div className="image-user">
                <img
                  style={{
                    borderRadius: '26px',
                    width: '40px',
                    height: '40px',
                  }}
                  src={
                    myUser.imageUser || myUser.pictrue
                      ? myUser.imageUser || myUser.picture
                      : 'https://www.kindpng.com/picc/m/22-223863_no-avatar-png-circle-transparent-png.png'
                  }
                  alt="avatar"
                />
              </div>
              <div className="description-user">
                <Button
                  id="basic-button"
                  color="primary"
                  aria-controls={open ? 'basic-menu' : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? 'true' : undefined}
                  onClick={handleClick}
                >
                  {(myUser.userName && myUser.userName.substring(0, 10)) ||
                    (myUser.nickname && myUser.nickname.substring(0, 10))}
                  ...
                </Button>
                <Menu
                  id="basic-menu"
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  MenuListProps={{
                    'aria-labelledby': 'basic-button',
                  }}
                >
                  {myUser.isAdmin && (
                    <MenuItem onClick={handleClose}>
                      <a href="http://localhost:3000/admin">Admin</a>
                    </MenuItem>
                  )}
                  <MenuItem
                    onClick={() => {
                      clearCart()
                      logout({ returnTo: window.location.origin })
                    }}
                  >
                    Đăng xuất
                  </MenuItem>
                </Menu>
              </div>
            </div>
          </div>
        </>
      ) : (
        <button
          type="button"
          className="auth-btn login"
          onClick={loginWithRedirect}
        >
          Đăng nhập
        </button>
      )}
    </div>
  )
}

export default CartButtons
