import React from 'react'
import { useProductsContext } from '../../context/products_context'
import { Link } from 'react-router-dom'
import './FeaturedProducts.scss'
import Error from '../Error'
import Loading from '../Loading'
import Product from '../Product/Product'

const FeaturedProducts = () => {
  const {
    products_loading: loading,
    products_error: error,
    featured_products: featured,
  } = useProductsContext()
  setTimeout(() => {
    if (loading) {
      return <Loading />
    }
  }, 3000)

  if (error) {
    return <Error />
  }

  return (
    <div className="section featurep">
      <div className="title">
        <h2>Sản phẩm nổi bật{console.log(featured)}</h2>
        <div className="underline"></div>
      </div>
      <div className="section-center featured">
        {featured.slice(0, 6).map((product) => {
          return <Product key={product.id} {...product} />
        })}
      </div>
      <Link to="/products" className="btn">
        Tất cả sản phẩm
      </Link>
    </div>
  )
}

export default FeaturedProducts
