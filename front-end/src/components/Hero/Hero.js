import React from 'react'
import './Hero.scss'
import { Link } from 'react-router-dom'

const Hero = () => {
  return (
    <section className="section-center hero">
      <article className="content">
        <h1>
          design your <br /> typing feeling a comfortable
        </h1>
        <p>
          Slogan “Bởi vì bàn tay bạn là vô giá” lần nữa khẳng định ComfySloth sẽ
          mang đến cho bạn những trải nghiệm công nghệ lành mạnh nhất với các
          sản phẩm cao cấp, bền bỉ và mang tính công thái học cao. Vì với chúng
          tôi, sản phẩm công nghệ không chỉ để dùng mà còn phải bảo vệ sức khỏe
          mỗi ngày.
        </p>
        <Link to="/products" className="btn hero-btn">
          Mua sắm ngay
        </Link>
      </article>
      <article className="img-container">
        <img
          src="https://www.phongcachxanh.vn/web/image/352528/Glorious-GMMK-Pro-Explosion-v2.png"
          alt="nice table"
          className="main-img"
        />
        <img
          src="https://d1vm37nfym7tjl.cloudfront.net/web/image/product.product/21190/image_1024/%5BGLO-ACC-P75-RK-G%5D%20N%C3%BAm%20xoay%20thay%20th%E1%BA%BF%20Glorious%20GMMK%20PRO%20Rotary%20Knob%20-%20Gold?unique=a9e8b67"
          alt="person working"
          className="accent-img"
        />
      </article>
    </section>
  )
}

export default Hero
