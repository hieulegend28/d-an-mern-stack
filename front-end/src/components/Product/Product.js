import React from 'react'
import './Product.scss'
import { formatPrice } from '../../constants/helpers'
import { FaSearch } from 'react-icons/fa'
import { Link } from 'react-router-dom'

const Product = ({ images, name, price, _id }) => {
  return (
    <div className="productss">
      <div className="container">
        <img src={images[0]} alt={name} />
        <Link to={`/products/${_id}`} className="link">
          <FaSearch />
        </Link>
      </div>

      <footer>
        <div>
          <h5>{name.substring(0, 50)}...</h5>
        </div>
        <p>{formatPrice(price)}</p>
      </footer>
    </div>
  )
}

export default Product
