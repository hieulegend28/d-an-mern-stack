import React from 'react'
import './Navbar.scss'
import logo from '../../assets/logo.svg'
import { FaBars } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import { links } from '../../constants/constants'
import CartButtons from '../CartButtons/CartButtons'
import { useUserContext } from '../../context/user_context'

const Nav = () => {
  const { singleUser: myUser } = useUserContext()

  return (
    <nav className="navbar">
      <div className="nav-center">
        <div className="nav-header">
          <Link to="/">
            <img src={logo} alt="comfy sloth" />
          </Link>
          <button type="button" className="nav-toggle">
            <FaBars />
          </button>
        </div>
        <ul className="nav-links">
          {links.map((link) => {
            const { id, text, url } = link
            return (
              <li key={id}>
                <Link to={url}>{text}</Link>
              </li>
            )
          })}
          {myUser && (
            <li>
              <Link to="/checkout">Thanh toán</Link>
            </li>
          )}
        </ul>
        <CartButtons />
      </div>
    </nav>
  )
}

export default Nav
