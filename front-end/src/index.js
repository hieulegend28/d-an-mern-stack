import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import { ProductsProvider } from './context/products_context'
import { FilterProvider } from './context/filter_context'
import { CartProvider } from './context/cart_context'
import { UserProvider } from './context/user_context'
import { Auth0Provider } from '@auth0/auth0-react'
import { CheckoutProvider } from './context/checkout_context'

// dev-vbobagow.us.auth0.com
// ahj5ZGOqYJOza2JQRSUS3wFOx0CKIFMG

ReactDOM.render(
  <Auth0Provider
    domain="dev-glguxmhc.us.auth0.com"
    clientId="EC6YPwrpyZGwOUK6ngSLAiDopbx0Rybu"
    redirectUri={window.location.origin}
  >
    <UserProvider>
      <ProductsProvider>
        <FilterProvider>
          <CartProvider>
            <CheckoutProvider>
              <App />
            </CheckoutProvider>
          </CartProvider>
        </FilterProvider>
      </ProductsProvider>
    </UserProvider>
  </Auth0Provider>,
  document.getElementById('root')
)
