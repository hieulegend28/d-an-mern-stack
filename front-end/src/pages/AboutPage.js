import React from 'react'
import styled from 'styled-components'
import { PageHero } from '../components'

const AboutPage = () => {
  return (
    <main>
      <PageHero title="Giới thiệu" />
      <Wrapper className="page section section-center">
        <img
          src="https://scontent.fsgn2-3.fna.fbcdn.net/v/t1.6435-9/199039289_3029461617290805_5136396402393101068_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=e3f864&_nc_ohc=oLzVe0jFKV4AX_BO0Fo&_nc_ht=scontent.fsgn2-3.fna&oh=00_AT92D1QuBCjCBjTNa8xiuINHCpBHRJ5QHN9heSjMjNjAGw&oe=627C8A78"
          alt="nice desk"
        />
        <article>
          <div className="title">
            <h2>Câu chuyện của chúng tôi</h2>
            <div className="underline"></div>
          </div>
          <p>
            Năm thi đại học, tổng điểm ba môn tôi chỉ được 10 điểm, còn con trai
            của bạn mẹ tôi được 27 điểm, cậu ta đến học tại trường đại học UIT,
            còn tôi chỉ có thể đi code dạo. Chín năm sau, mẹ của cậu ta chạy đến
            khoe khoang với tôi và mẹ tôi rằng con trai bà ta đang đi phỏng vấn
            vào chức giám đốc lương tháng vài chục triệu… Còn tôi, lại đang
            nghĩ: có nên tuyển dụng cậu ta không.
          </p>
        </article>
      </Wrapper>
    </main>
  )
}

const Wrapper = styled.section`
  display: grid;
  gap: 4rem;
  img {
    width: 100%;
    display: block;
    border-radius: var(--radius);
    height: 500px;
    object-fit: cover;
  }
  p {
    line-height: 2;
    max-width: 45em;
    margin: 0 auto;
    margin-top: 2rem;
    color: var(--clr-grey-5);
  }
  .title {
    text-align: left;
  }
  .underline {
    margin-left: 0;
  }
  @media (min-width: 992px) {
    grid-template-columns: 1fr 1fr;
  }
`
export default AboutPage
