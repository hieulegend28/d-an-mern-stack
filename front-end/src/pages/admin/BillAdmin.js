import MaterialTable from '@material-table/core'
import moment from 'moment'
import { useState } from 'react'
import FlashMessage from '../../components/Admin/flash-message'
import { useCheckoutContext } from '../../context/checkout_context'
import { formatPrice } from '../../constants/helpers'

const BillAdmin = () => {
  const { bills } = useCheckoutContext()
  const [noti, setNoti] = useState({ message: '', severity: '', opens: false })

  function flashNotification(message, severity) {
    setNoti({
      message: message,
      severity: severity,
      opens: true,
    })
    setTimeout(() => {
      setNoti({
        ...noti,
        opens: false,
      })
    }, 3000)
  }

  const columns = [
    {
      title: 'STT',
      render: (rowData) => <span>{rowData.tableData.index + 1}</span>,
      editable: false,
      width: '10%',
    },
    {
      title: 'Tên',
      field: 'user.fullName',
    },
    {
      title: 'Số ĐT',
      field: 'user.phoneNumber',
    },
    {
      title: 'Địa chỉ',
      field: 'user.address',
    },
    {
      title: 'Tổng',
      render: (rowData) => <span>{formatPrice(rowData.total)}</span>,
    },
    {
      title: 'Ngày tạo',
      field: 'createdAt',
      render: (rowData) => (
        <span>{moment(rowData.createdAt).format('DD/MM/YYYY')}</span>
      ),
      editable: false,
    },
  ]

  return (
    <div>
      <h1 style={{ textAlign: 'center', marginBottom: '30px' }}>
        Quản lý hóa đơn
      </h1>
      <MaterialTable
        title="Danh sách hóa đơn"
        data={bills}
        columns={columns}
        localization={{
          header: {
            actions: 'Hành động',
          },
          toolbar: {
            searchPlaceholder: 'Tìm kiếm',
          },
          body: {
            editRow: {
              deleteText: 'Bạn chắc chắn muốn xóa danh mục này?',
            },
          },
        }}
        options={{
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            backgroundColor: '#c5a491',
            color: '#FFF',
          },
        }}
        detailPanel={[
          {
            tooltip: 'Show Name',
            render: (rowData) => {
              return (
                <MaterialTable
                  title="Chi tiết hóa đơn"
                  data={rowData.rowData.product}
                  options={{
                    search: false,
                  }}
                  columns={[
                    {
                      title: 'STT',
                      width: '10%',
                      render: (rowData) => (
                        <span>{rowData.tableData.index + 1}</span>
                      ),
                    },
                    {
                      title: 'Tên',
                      field: 'name',
                    },
                    {
                      title: 'Danh mục',
                      field: 'category.categoryName',
                    },
                    {
                      title: 'Switch',
                      field: 'switchs',
                    },
                    {
                      title: 'Màu',
                      render: (rowData) => (
                        <div
                          style={{
                            backgroundColor: rowData.color,
                            width: '30px',
                            height: '30px',
                            borderRadius: '50%',
                            border: '1px solid #222',
                          }}
                        ></div>
                      ),
                    },
                    {
                      title: 'Giá',
                      render: (rowData) => (
                        <span>{formatPrice(rowData.price)}</span>
                      ),
                    },
                    {
                      title: 'Số lượng',
                      field: 'amount',
                    },
                  ]}
                />
              )
            },
          },
        ]}
      />
      {noti.opens && <FlashMessage {...noti} />}
    </div>
  )
}

export default BillAdmin
