import MaterialTable from '@material-table/core'
import moment from 'moment'
import { useEffect, useState } from 'react'
import axios from 'axios'
import FlashMessage from '../../components/Admin/flash-message'
import { formatPrice } from '../../constants/helpers'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Modal from '@mui/material/Modal'
import Grid from '@mui/material/Grid'
import TextField from '@mui/material/TextField'
import { useProductsContext } from '../../context/products_context'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import { categories_url as url } from '../../constants/constants'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #c5a491',
  boxShadow: 24,
  p: 4,
}

const ProductAdmin = () => {
  const { products } = useProductsContext()
  const [noti, setNoti] = useState({ message: '', severity: '', opens: false })
  const [open, setOpen] = useState(false)
  const [categories, setCategories] = useState([])
  const [singleProduct, setSingleProduct] = useState({
    category: {
      categoryName: '',
    },
  })
  const [age, setAge] = useState('')
  async function fetchdata() {
    const response = await axios.get(url)
    setCategories(response.data.filter((x) => x.enableCategory === true))
    return response
  }
  useEffect(() => {
    fetchdata()
  }, [])
  const handleChange = (event) => {
    setSingleProduct({
      ...singleProduct,
      category: { ...singleProduct.category, categoryName: event.target.value },
    })
  }
  console.log(singleProduct.category.categoryName)
  const handleOpen = (id) => {
    const findProduct = products.find((x) => x._id === id)
    setSingleProduct(findProduct)
    setOpen(true)
  }

  const handleClose = () => setOpen(false)

  const listProducts = products.filter(
    (x) => x.category.enableCategory === true
  )

  function flashNotification(message, severity) {
    setNoti({
      message: message,
      severity: severity,
      opens: true,
    })
    setTimeout(() => {
      setNoti({
        ...noti,
        opens: false,
      })
    }, 3000)
  }

  const columns = [
    {
      title: 'STT',
      render: (rowData) => <span>{rowData.tableData.index + 1}</span>,
      editable: false,
      width: '10%',
    },
    {
      title: 'Ảnh',
      field: 'images',
      render: (rowData) => (
        <img style={{ width: '120px' }} src={rowData.images[0]} alt="" />
      ),
    },
    {
      title: 'Tên',
      field: 'name',
      render: (rowData) => <span>{rowData.name.substring(0, 40)}...</span>,
    },
    {
      title: 'Giá',
      field: 'price',
      render: (rowData) => <span>{formatPrice(rowData.price)}</span>,
    },
    {
      title: 'Số lượng',
      field: 'countInStock',
    },
    {
      title: 'Thương hiệu',
      field: 'brand',
    },

    {
      title: 'Ngày tạo',
      field: 'createdAt',
      render: (rowData) => (
        <span>{moment(rowData.createdAt).format('DD/MM/YYYY')}</span>
      ),
      editable: false,
    },
  ]

  async function addCategory(newData) {
    // const olddata = categories.find(
    //   (x) =>
    //     x.categoryName === newData.categoryName && x.enableCategory === true
    // )
    // if (!olddata) {
    //   await axios
    //     .post('http://localhost:5000/api/category/create', newData)
    //     .then((res) => {
    //       flashNotification('Tạo mới danh mục thành công', 'success')
    //       fetchdata()
    //     })
    //     .catch((err) => console.log(err.response.data.message))
    // } else {
    //   flashNotification('Tên danh mục đã tồn tại', 'error')
    // }
  }

  async function deteleProduct(oldData) {
    await axios
      .post('http://localhost:5000/api/product/delete', { _id: oldData._id })
      .then((res) => {
        flashNotification('Xóa sản phẩm thành công', 'success')
      })
      .catch((err) => console.log(err))
  }

  async function updateCategory(newData) {
    // const olddata = categories.find(
    //   (x) =>
    //     x.categoryName === newData.categoryName && x.enableCategory === true
    // )
    // if (!olddata) {
    //   await axios
    //     .post('http://localhost:5000/api/category/update', newData)
    //     .then((res) => {
    //       flashNotification('Cập nhật danh mục thành công', 'success')
    //       fetchdata()
    //     })
    //     .catch((err) => console.log(err))
    // } else {
    //   flashNotification('Tên danh mục đã tồn tại', 'error')
    // }
  }

  return (
    <div>
      <h1 style={{ textAlign: 'center', marginBottom: '30px' }}>
        Quản lý sản phẩm
      </h1>
      <MaterialTable
        title="Danh sách sản phẩm"
        data={listProducts}
        columns={columns}
        localization={{
          header: {
            actions: 'Hành động',
          },
          toolbar: {
            searchPlaceholder: 'Tìm kiếm',
          },
          body: {
            editRow: {
              deleteText: 'Bạn chắc chắn muốn xóa danh mục này?',
            },
          },
        }}
        options={{
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            backgroundColor: '#c5a491',
            color: '#FFF',
          },
        }}
        editable={{
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                deteleProduct(oldData)
                resolve()
              }, 500)
            }),
        }}
        actions={[
          {
            icon: 'edit',
            tooltip: 'Edit product',
            onClick: (event, rowData) => handleOpen(rowData._id),
          },
        ]}
      />
      {noti.opens && <FlashMessage {...noti} />}

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Chỉnh sửa sản phẩm
          </Typography>
          <br />
          <form>
            <Grid container spacing={2}>
              <Grid item xs={6} sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Tên sản phẩm"
                  defaultValue={singleProduct.name}
                  fullWidth
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">
                    Danh mục
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={singleProduct.category.categoryName}
                    label="Danh mục"
                    onChange={handleChange}
                  >
                    {categories.map((x) => {
                      return (
                        <MenuItem key={x._id} value={x.categoryName}>
                          {x.categoryName}
                        </MenuItem>
                      )
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={6} sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Giá"
                  defaultValue={singleProduct.price}
                  fullWidth
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Số lượng"
                  defaultValue={singleProduct.countInStock}
                  fullWidth
                />
              </Grid>

              <Grid item xs={6} sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Bảo hành"
                  defaultValue={singleProduct.insurance}
                  fullWidth
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <TextField
                  required
                  id="outlined-required"
                  label="Size"
                  defaultValue={singleProduct.size}
                  fullWidth
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <TextField
                  id="outlined-multiline-static"
                  label="Mổ tả"
                  multiline
                  rows={4}
                  defaultValue={singleProduct.description}
                  fullWidth
                />
              </Grid>
            </Grid>
          </form>
        </Box>
      </Modal>
    </div>
  )
}

export default ProductAdmin
