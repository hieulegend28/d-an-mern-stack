import MaterialTable from '@material-table/core'
import moment from 'moment'
import { useEffect, useState } from 'react'
import axios from 'axios'
import { categories_url as url } from '../../constants/constants'
import FlashMessage from '../../components/Admin/flash-message'

const CategoryAdmin = () => {
  const [categories, setCategories] = useState([])
  const [noti, setNoti] = useState({ message: '', severity: '', opens: false })
  async function fetchdata() {
    const response = await axios.get(url)
    setCategories(response.data.filter((x) => x.enableCategory === true))
    return response
  }
  useEffect(() => {
    fetchdata()
  }, [])

  function flashNotification(message, severity) {
    setNoti({
      message: message,
      severity: severity,
      opens: true,
    })
    setTimeout(() => {
      setNoti({
        ...noti,
        opens: false,
      })
    }, 3000)
  }

  const columns = [
    {
      title: 'STT',
      render: (rowData) => <span>{rowData.tableData.index + 1}</span>,
      editable: false,
    },
    {
      title: 'Tên',
      field: 'categoryName',
      align: 'justify',
    },
    {
      title: 'Ngày tạo',
      field: 'createdAt',
      render: (rowData) => (
        <span>{moment(rowData.createdAt).format('DD/MM/YYYY')}</span>
      ),
      editable: false,
    },
  ]

  async function addCategory(newData) {
    const olddata = categories.find(
      (x) =>
        x.categoryName === newData.categoryName && x.enableCategory === true
    )
    if (!olddata) {
      await axios
        .post('http://localhost:5000/api/category/create', newData)
        .then((res) => {
          flashNotification('Tạo mới danh mục thành công', 'success')
          fetchdata()
        })
        .catch((err) => console.log(err.response.data.message))
    } else {
      flashNotification('Tên danh mục đã tồn tại', 'error')
    }
  }

  async function deteleCategory(oldData) {
    oldData.enableCategory = false
    await axios
      .post('http://localhost:5000/api/category/delete', oldData)
      .then((res) => {
        flashNotification('Xóa danh mục thành công', 'success')
        fetchdata()
      })
      .catch((err) => console.log(err))
  }

  async function updateCategory(newData) {
    const olddata = categories.find(
      (x) =>
        x.categoryName === newData.categoryName && x.enableCategory === true
    )
    if (!olddata) {
      await axios
        .post('http://localhost:5000/api/category/update', newData)
        .then((res) => {
          flashNotification('Cập nhật danh mục thành công', 'success')
          fetchdata()
        })
        .catch((err) => console.log(err))
    } else {
      flashNotification('Tên danh mục đã tồn tại', 'error')
    }
  }

  return (
    <div>
      <h1 style={{ textAlign: 'center', marginBottom: '30px' }}>
        Quản lý danh mục
      </h1>
      <MaterialTable
        title="Danh sách danh mục"
        data={categories}
        columns={columns}
        localization={{
          header: {
            actions: 'Hành động',
          },
          toolbar: {
            searchPlaceholder: 'Tìm kiếm',
          },
          body: {
            editRow: {
              deleteText: 'Bạn chắc chắn muốn xóa danh mục này?',
            },
          },
        }}
        options={{
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            backgroundColor: '#c5a491',
            color: '#FFF',
          },
        }}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                addCategory(newData)
                resolve()
              }, 500)
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updateCategory(newData)
                resolve()
              }, 500)
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                deteleCategory(oldData)
                resolve()
              }, 500)
            }),
        }}
      />
      {noti.opens && <FlashMessage {...noti} />}
    </div>
  )
}

export default CategoryAdmin
