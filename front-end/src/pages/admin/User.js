import MaterialTable from '@material-table/core'
import moment from 'moment'
import { useState } from 'react'
import axios from 'axios'
import FlashMessage from '../../components/Admin/flash-message'
import { useUserContext } from '../../context/user_context'

const User = () => {
  const { users } = useUserContext()
  const [noti, setNoti] = useState({ message: '', severity: '', opens: false })
  console.log(users)
  function flashNotification(message, severity) {
    setNoti({
      message: message,
      severity: severity,
      opens: true,
    })
    setTimeout(() => {
      setNoti({
        ...noti,
        opens: false,
      })
    }, 3000)
  }

  const columns = [
    {
      title: 'STT',
      render: (rowData) => <span>{rowData.tableData.index + 1}</span>,
      editable: false,
      width: '10%',
    },
    {
      title: 'Ảnh',
      field: 'imageUser',
      editable: false,
      width: '20%',
      render: (rowData) => (
        <img
          style={{ width: '70px', borderRadius: '50%' }}
          src={rowData.imageUser}
          alt=""
        />
      ),
    },
    {
      title: 'Tên',
      field: 'userName',
      editable: false,
      width: '20%',
    },
    {
      title: 'Email',
      field: 'email',
      editable: false,
      width: '20%',
    },
    {
      title: 'Ngày tham gia',
      field: 'createdAt',
      render: (rowData) => (
        <span>{moment(rowData.createdAt).format('DD/MM/YYYY')}</span>
      ),
      editable: false,
      width: '20%',
    },
    {
      title: 'Đăng nhập bằng',
      field: 'sub',
      editable: false,
      width: '20%',
    },
    {
      title: 'Admin',
      field: 'isAdmin',
      width: '20%',
      lookup: { true: 'true', false: 'false' },
      render: (rowData) => <div>{rowData.isAdmin + ''}</div>,
    },
  ]

  async function updateUser(newData) {
    console.log(newData)
    await axios
      .post('http://localhost:5000/api/auth/update', newData)
      .then((res) => {
        flashNotification('Cập nhật người dùng thành công', 'success')
      })
      .catch((err) => console.log(err))
  }

  return (
    <div>
      <h1 style={{ textAlign: 'center', marginBottom: '30px' }}>
        Quản lý người dùng
      </h1>
      <MaterialTable
        title="Danh sách người dùng"
        data={users}
        columns={columns}
        localization={{
          header: {
            actions: 'Hành động',
          },
          toolbar: {
            searchPlaceholder: 'Tìm kiếm',
          },
          body: {
            editRow: {
              deleteText: 'Bạn chắc chắn muốn xóa danh mục này?',
            },
          },
        }}
        options={{
          actionsColumnIndex: -1,
          addRowPosition: 'first',
          headerStyle: {
            backgroundColor: '#c5a491',
            color: '#FFF',
          },
        }}
        editable={{
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                updateUser(newData)
                resolve()
              }, 500)
            }),
        }}
      />
      {noti.opens && <FlashMessage {...noti} />}
    </div>
  )
}

export default User
