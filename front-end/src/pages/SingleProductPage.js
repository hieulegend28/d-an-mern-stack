import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { useProductsContext } from '../context/products_context'
import { products_url as url } from '../constants/constants'
import { formatPrice } from '../constants/helpers'
import {
  Loading,
  Error,
  ProductImages,
  AddToCart,
  Stars,
  PageHero,
  ProductDetail,
} from '../components'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import { TabContext, TabList, TabPanel } from '@mui/lab'

const SingleProductPage = () => {
  const [value, setValue] = useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  const { id } = useParams()
  const history = useNavigate()
  const {
    single_product_loading: loading,
    single_product_error: error,
    single_product: product,
    fetchSingleProduct,
  } = useProductsContext()

  useEffect(() => {
    fetchSingleProduct(`${url}/${id}`)
  }, [])

  useEffect(() => {
    if (error) {
      setTimeout(() => {
        history.push('/')
      }, 1000)
    }
  }, [error, history])

  if (loading) {
    return <Loading />
  }

  if (error) {
    return <Error type="single-product" />
  }
  const {
    name,
    price,
    description,
    countInStock,
    stars,
    reviews,
    category,
    brand,
    images,
    status,
    size,
    pressingForce,
    Compatible,
    switchColor,
  } = product

  return (
    <Wrapper>
      <PageHero title={name ? name.substring(0, 40) : name} product />
      <div className="section section-center page">
        <Link to="/products" className="btn">
          Quay về danh sách sản phẩm
        </Link>
        <div className="product-center">
          {images && <ProductImages images={images} />}
          <section className="content">
            <h2>{name}</h2>
            <Stars stars={stars} reviews={reviews} />
            <h5 className="price">{price ? formatPrice(price) : 0}</h5>
            <p className="desc">{description}</p>
            {category && category.categoryName === 'Bàn phím cơ' && (
              <p className="info">
                <span className="lable">Số lượng : </span>
                {countInStock > 0 ? `Sản phẩm (${countInStock})` : 'Hết hàng'}
              </p>
            )}
            {category && category.categoryName === 'Switch bấm' && (
              <>
                <p className="info">
                  <span className="lable">Số lượng : </span>
                  {size}
                </p>
                <p className="info">
                  <span className="lable">Lực nhấn : </span>
                  {pressingForce}
                </p>
                <p className="info">
                  <span className="lable">Tương thích : </span>
                  {Compatible}
                </p>
                <p className="info">
                  <span className="lable">Loại Switch : </span>
                  {switchColor}
                </p>
              </>
            )}

            <p className="info">
              <span className="lable">Thương hiệu : </span>
              {brand}
            </p>
            <p className="info">
              <span className="lable">Tình trạng : </span>
              {status}
            </p>
            <hr />
            {countInStock > 0 && <AddToCart product={product} />}
          </section>
        </div>
      </div>
      <div className="section section-center page">
        <Box sx={{ width: '100%', typography: 'body1' }}>
          <TabContext value={value}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <TabList
                onChange={handleChange}
                aria-label="lab API tabs example"
                centered
              >
                <Tab label="Thông số" value="1" />
                <Tab label="Đánh giá" value="2" />
              </TabList>
            </Box>
            <TabPanel value="1">
              <ProductDetail />
            </TabPanel>
            <TabPanel value="2">Item Three</TabPanel>
          </TabContext>
        </Box>
      </div>
    </Wrapper>
  )
}

const Wrapper = styled.main`
  .product-center {
    display: grid;
    gap: 4rem;
    margin-top: 2rem;
  }
  .price {
    color: var(--clr-primary-5);
  }
  .desc {
    line-height: 2;
    max-width: 45em;
    font-weight: 500;
  }
  .info {
    width: 600px;
    display: grid;
    grid-template-columns: 125px 1fr;
  }
  .lable {
    font-weight: 900;
  }

  @media (min-width: 992px) {
    .product-center {
      grid-template-columns: 1fr 1fr;
      align-items: center;
    }
    .price {
      font-size: 1.25rem;
    }
  }
  .css-ahj2mt-MuiTypography-root {
    font-family: 'Lora, serif';
  }
`

export default SingleProductPage
