import React, { useLayoutEffect, useState } from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Navbar, Footer } from './components'
import './assets/libs/boxicons-2.1.1/css/boxicons.min.css'
import './scss/App.scss'
import CategoryAdmin from './pages/admin/CategoryAdmin'
import ProductAdmin from './pages/admin/ProductAdmin'
import MainLayout from './layout/MainLayout'
import {
  Home,
  SingleProduct,
  Cart,
  Checkout,
  Error,
  About,
  Products,
  AuthWrapper,
} from './pages'
import User from './pages/admin/User'
import { useUserContext } from './context/user_context'
import BillAdmin from './pages/admin/BillAdmin'

function App() {
  const { singleUser } = useUserContext()

  const [curPath, setCurPath] = useState(window.location.pathname.split('/')[1])

  useLayoutEffect(() => {
    setCurPath(window.location.pathname.split('/')[1])
  }, [curPath])
  console.log(singleUser)

  return (
    <AuthWrapper>
      <Router>
        {curPath === 'admin' && singleUser && singleUser.isAdmin ? (
          <Routes>
            <Route path="/" element={<App />} />
            <Route path="/admin" element={<MainLayout />}>
              <Route index element={<CategoryAdmin />} />
              <Route path="products" element={<ProductAdmin />} />
              <Route path="bills" element={<BillAdmin />} />
              <Route path="users" element={<User />} />
              <Route path="stats" element={<CategoryAdmin />} />
            </Route>
          </Routes>
        ) : (
          <>
            <Navbar />

            <Routes>
              <Route path="admin" element={<Home />} />
              <Route index element={<Home />} />

              <Route path="about" element={<About />} />

              <Route path="cart" element={<Cart />} />

              <Route path="products" element={<Products />} />

              <Route path="products/:id" element={<SingleProduct />} />
              {singleUser ? (
                <Route path="checkout" element={<Checkout />} />
              ) : (
                <Route path="checkout" element={<Home />} />
              )}

              <Route path="*" element={<Error />} />
            </Routes>

            {/* Footer */}
            <Footer />
          </>
        )}
      </Router>
    </AuthWrapper>
  )
}

export default App
