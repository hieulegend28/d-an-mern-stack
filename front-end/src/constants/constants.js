import React from 'react'
import { GiCompass, GiDiamondHard, GiStabbedNote } from 'react-icons/gi'
export const links = [
  {
    id: 1,
    text: 'trang chủ',
    url: '/',
  },
  {
    id: 2,
    text: 'giới thiệu',
    url: '/about',
  },
  {
    id: 3,
    text: 'sản phẩm',
    url: '/products',
  },
]

export const services = [
  {
    id: 1,
    icon: <GiCompass />,
    title: 'Nhiệm Vụ',
    text: 'Với mong muốn đem trải nghiệm bàn phím cơ đến với mọi người, chúng tôi luôn cố gắng hết mình để quảng bá và xây dựng một cộng đồng người dùng đông đảo.',
  },
  {
    id: 2,
    icon: <GiDiamondHard />,
    title: 'Trách Nhiệm',
    text: 'Chúng tôi luôn cam kết sản phẩm của mình là hàng chính hãng 100%, đền tiền gấp 3 nếu bạn phát hiện sản phẩm của chúng tôi là hàng nhái.',
  },
  {
    id: 3,
    icon: <GiStabbedNote />,
    title: 'Lịch Sử',
    text: 'Với lịch sử phát triển hơn 10 năm, hiện tại đã có hơn 500 chi nhánh trên toàn quốc. Chúng tôi tự tin mình là nhà phân phối sản phẩm bàn phím cơ hàng đầu tại Việt Nam.',
  },
]

export const products_url = 'http://localhost:5000/api/product'
export const categories_url = 'http://localhost:5000/api/category'
export const users_url = 'http://localhost:5000/api/auth/user'
