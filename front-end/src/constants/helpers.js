export const formatPrice = (number) => {
  return number.toLocaleString('it-IT', { style: 'currency', currency: 'VND' })
}

export const getUniqueValues = (data, type) => {
  let unique = []
  if (type === 'category') {
    unique = data.map((item) => item[type].categoryName)
  } else {
    unique = data.map((item) => item[type])

    if (type === 'colors') {
      unique = unique.flat()
    }

    if (type === 'styleSwitch') {
      unique = unique.flat()
    }
  }

  return ['Tất cả', ...new Set(unique)]
}
