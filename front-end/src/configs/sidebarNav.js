const sidebarNav = [
  {
    link: '/admin',
    section: 'admin',
    icon: <i className="bx bx-home-alt"></i>,
    text: 'Danh mục',
  },
  {
    link: '/admin/products',
    section: '/products',
    icon: <i className="bx bx-cube"></i>,
    text: 'Sản phẩm',
  },
  {
    link: '/admin/users',
    section: '/users',
    icon: <i className="bx bx-user"></i>,
    text: 'Người dùng',
  },
  {
    link: '/admin/bills',
    section: '/bills',
    icon: <i className="bx bx-receipt"></i>,
    text: 'Hóa đơn',
  },
  {
    link: '/admin/stats',
    section: '/stats',
    icon: <i className="bx bx-line-chart"></i>,
    text: 'Thống kê',
  },
]

export default sidebarNav
