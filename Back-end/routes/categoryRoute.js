const express = require('express')
const router = express.Router()
const categoryController = require('../controller/categoryController')

//@desc GET all category form db
//route GET api/category
router.get('/', categoryController.getAllCategory)

//@desc GET a category by ID from db
//route GET api/category/id=
// router.get('/:id', categoryController.getcategoryById)
//@desc POST a category to db
//route POST api/category/create
router.post('/create', categoryController.createCategory)
// //@desc DELETE a category by ID from db
// //route POST api/category/delete
router.post('/delete', categoryController.deleteCategory)

//@desc UPDATE a category
//route POST api/category/update
router.post('/update', categoryController.updateCategory)

module.exports = router
