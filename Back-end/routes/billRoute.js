const express = require('express')
const router = express.Router()
const billController = require('../controller/billController')

router.get('/', billController.getAllBill)

//@desc POST a category to db
//route POST api/category/create
router.post('/create', billController.creatBill)

module.exports = router
