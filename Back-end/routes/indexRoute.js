const authRouter = require('./authRoute')
const productRouter = require('./productRoute')
const categoryRouter = require('./categoryRoute')
const billRouter = require('./billRoute')

const indexRoute = (app) => {
  app.use('/api/auth', authRouter)
  app.use('/api/product', productRouter)
  app.use('/api/category', categoryRouter)
  app.use('/api/bill', billRouter)
}

module.exports = indexRoute
