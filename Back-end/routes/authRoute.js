const express = require('express')
const router = express.Router()
const userController = require('../controller/userController')

router.get('/', (req, res) => res.send('USER ROUTE'))

// @route POST api/auth/login

router.post('/login', userController.loginUser)

// @route GET api/auth/user
router.get('/user', userController.getUser)

// @route POST api/auth/update
router.post('/update', userController.updateUser)

// @route GET api/auth/getuser
router.post('/getuser', userController.getUserByEmail)

module.exports = router
