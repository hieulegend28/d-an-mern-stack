const express = require('express')
const router = express.Router()
const productController = require('../controller/productController')
const verify = require('../middleware/auth')
//@desc GET all product form db
//route GET api/product
router.get('/', productController.getAllProduct)

//@desc GET a product by ID from db
//route GET api/product/id=
router.get('/:id', productController.getProductById)
//@desc POST a product to db
//route POST api/product/create
router.post('/create', productController.createProduct)
//@desc DELETE a product by ID from db
//route POST api/product/delete
router.post('/delete', productController.deleteProduct)
//@desc UPDATE a product
//route POST api/product/update
router.post('/update', productController.updateProduct)

module.exports = router
