const Bill = require('../models/Bill')

class billController {
  async getAllBill(req, res) {
    try {
      const bills = await Bill.find({}).populate({
        path: 'user',
        select: 'fullName phoneNumber address email',
      })
      res.json(bills)
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'Server error!' })
    }
  }

  async creatBill(req, res) {
    try {
      const newBill = new Bill(req.body)
      await newBill.save()
      return res
        .status(200)
        .json({ success: true, message: 'Crate bill success' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'Server error!' })
    }
  }
}

module.exports = new billController()
