const User = require('../models/User')
const axios = require('axios')
var request = require('request')
const { find } = require('../models/User')
var options = {
  method: 'POST',
  url: 'https://dev-glguxmhc.us.auth0.com/oauth/token',
  headers: { 'content-type': 'application/json' },
  body: '{"client_id":"CdMHsrF1wo6YfYNABOawhJII8UcpeKZR","client_secret":"jOK2cDuA9XgNghP9IjMB30cYBndhhrUxqvQ50bMFebnSW5siQrBtq-PEaL6oFLsL","audience":"https://dev-glguxmhc.us.auth0.com/api/v2/","grant_type":"client_credentials"}',
}
var listUser = []
request(options, async function (error, response, body) {
  if (error) throw new Error(error)

  var options = {
    method: 'GET',
    url: 'https://dev-glguxmhc.us.auth0.com/api/v2/users',
    headers: { authorization: 'Bearer ' + JSON.parse(body).access_token },
  }

  await axios
    .request(options)
    .then(function (response) {
      listUser = response.data
    })
    .catch(function (error) {
      console.error(error)
    })
})

class userController {
  //Login user
  async loginUser(req, res) {
    const user = await User.findOne({
      email: req.body.email,
      sub: req.body.sub,
    })
    console.log(user)
    try {
      const findUser = listUser.find(
        (x) =>
          x.email === req.body.email && x.user_id.split('|')[0] === req.body.sub
      )
      if (findUser) {
        const newUser = {
          userName: findUser.nickname,
          email: findUser.email,
          imageUser: findUser.picture,
          lastLogin: findUser.last_login,
          accessTime: findUser.logins_count,
          sub: findUser.user_id.split('|')[0],
        }
        if (!user) {
          const addUser = new User(newUser)
          await addUser.save()
        } else {
          await User.updateOne({ _id: user._id }, newUser)
        }
      }

      const lists = await User.find({})
      return res.json(lists)
    } catch (error) {
      console.log(error)
      res.status(500).json({ success: false, message: 'Internal server error' })
    }
  }

  async getUser(req, res) {
    try {
      const users = await User.find({})
      return res.json(users)
    } catch (error) {
      console.log(error)
      res.status(500).json({ success: false, message: 'Internal server error' })
    }
  }

  async updateUser(req, res) {
    try {
      const user = await User.findOne({ _id: req.body._id })
      if (!user) {
        return res
          .status(400)
          .json({ success: false, message: 'User not exist!' })
      }
      await User.updateOne({ _id: req.body._id }, req.body)
      res.json({
        success: true,
        message: 'update user success ' + req.body._id,
      })
    } catch (error) {
      console.log(error)
      res.status(500).json({ success: false, message: 'Internal server error' })
    }
  }

  async getUserByEmail(req, res) {
    try {
      const user = await User.findOne({ email: req.body.email })
      return res.json(user)
    } catch (error) {
      console.log(error)
      res.status(500).json({ success: false, message: 'Internal server error' })
    }
  }
}

module.exports = new userController()
