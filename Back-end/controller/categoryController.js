const Category = require('../models/category')

class categoryController {
  async getAllCategory(req, res) {
    try {
      const categories = await Category.find({})
      res.json(categories)
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'Server error!' })
    }
  }

  async createCategory(req, res) {
    try {
      const newCategory = new Category(req.body)
      const category = await Category.findOne({
        categoryName: newCategory.categoryName,
        enableCategory: true,
      })
      if (category)
        return res
          .status(400)
          .json({ success: false, message: 'Category already exist' })

      await newCategory.save()
      return res.json({ success: true, message: 'create category success' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'Create error!' })
    }
  }

  async updateCategory(req, res) {
    try {
      const category = await Category.findOne({ _id: req.body._id })
      if (!category) {
        return res
          .status(400)
          .json({ success: false, message: 'Category does not exist!' })
      }
      await Category.updateOne({ _id: req.body._id }, req.body)
      res.json({
        success: true,
        message: 'update category success ' + req.body._id,
      })
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'update error!' })
    }
  }

  async deleteCategory(req, res) {
    try {
      const category = await Category.findOne({ _id: req.body._id })
      if (!category) {
        return res
          .status(400)
          .json({ success: false, message: 'Category not exist!' })
      }
      await Category.updateOne({ _id: req.body._id }, req.body)
      res.json({
        success: true,
        message: 'delete category success ' + req.body._id,
      })
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'delete error!' })
    }
  }
}

module.exports = new categoryController()
