const Product = require('../models/Product')

class productController {
  async getAllProduct(req, res) {
    try {
      const products = await Product.find({}).populate('category')
      res.json(products)
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'Server error!' })
    }
  }

  async getProductById(req, res) {
    try {
      const product = await Product.findById(req.params.id).populate('category')
      res.json(product)
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'Server error!' })
    }
  }

  async createProduct(req, res) {
    try {
      const newProduct = new Product(req.body)
      const product = await Product.findOne({ name: newProduct.name })
      if (product)
        return res
          .status(400)
          .json({ success: false, message: 'Product already exist' })

      await newProduct.save()
      return res.json({ success: true, message: 'create product success' })
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'Server error!' })
    }
  }

  async deleteProduct(req, res) {
    try {
      const product = await Product.findOne({ _id: req.body._id })
      if (!product) {
        return res
          .status(400)
          .json({ success: false, message: 'Product not exist!' })
      }
      await Product.deleteOne({ _id: req.body._id })
      res.json({
        success: true,
        message: 'delete product success ' + req.body._id,
      })
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'delete error!' })
    }
  }

  async updateProduct(req, res) {
    try {
      const product = await Product.findOne({ _id: req.body._id })
      if (!product) {
        return res
          .status(400)
          .json({ success: false, message: 'Product not exist!' })
      }
      await Product.updateOne({ _id: req.body._id }, req.body)
      res.json({
        success: true,
        message: 'update product success ' + req.body._id,
      })
    } catch (error) {
      console.error(error)
      res.status(500).json({ success: false, message: 'update error!' })
    }
  }
}

module.exports = new productController()
