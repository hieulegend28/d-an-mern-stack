const express = require('express')
const connectDB = require('./connectDB/db')
const indexRoute = require('./routes/indexRoute')
var cors = require('cors')

connectDB()
const app = express()
app.use(cors())
app.use(express.json())
indexRoute(app)
const PORT = 5000
app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
