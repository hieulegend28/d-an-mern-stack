const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  userName: {
    type: String,
  },
  fullName: {
    type: String,
    default: '',
  },
  email: {
    type: String,
    required: true,
  },
  imageUser: {
    type: String,
  },
  lastLogin: {
    type: Date,
  },
  accessTime: {
    type: Number,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  phoneNumber: {
    type: String,
    default: '',
  },
  address: {
    type: String,
    default: '',
  },
  sub: {
    type: String,
  },
  remember: {
    type: Boolean,
    default: false,
  },
  createAt: {
    type: Date,
    default: Date.now(),
  },
})

module.exports = mongoose.model('users', UserSchema)
