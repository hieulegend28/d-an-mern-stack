const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Product = new Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    insurance: {
      type: String,
    },
    category: {
      type: Schema.Types.ObjectId,
      ref: 'category',
    },
    description: {
      type: String,
    },
    size: {
      type: String,
    },
    images: {
      type: Array,
      require: true,
    },
    colors: {
      type: Array,
    },
    price: {
      type: Number,
      require: true,
    },
    featured: {
      type: Boolean,
    },
    brand: {
      type: String,
    },
    status: {
      type: String,
      enum: ['Tạm thời hết hàng', 'Còn hàng', 'Sắp ra mắt'],
    },
    styleSwitch: {
      type: Array,
      required: true,
    },
    freeShip: {
      type: Boolean,
    },
    Compatible: {
      type: String,
    },
    pressingForce: {
      type: String,
    },
    switchColor: {
      type: String,
    },
    countInStock: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
)

module.exports = mongoose.model('product', Product)
