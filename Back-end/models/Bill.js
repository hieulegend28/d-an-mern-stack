const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Bill = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'users',
    },
    product: [],
    total: {
      type: Number,
    },
    count: {
      type: Number,
    },
  },
  {
    timestamps: true,
  }
)
module.exports = mongoose.model('bill', Bill)
